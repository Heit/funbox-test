import React from 'react';
import { useRef, useState, useEffect } from 'react';

import '../styles/Card.css';

function Card(props) {

    const[isEnabled, setIsEnabled] = useState(props.isEnabled);
    const[isSelected, setIsSelected] = useState(props.isSelected);
    const[isHovered, setIsHovered] = useState(false);
    const[isOnceSelected, setIsOnceSelected] = useState(false);
    const[isOnceLeaved, setIsOnceLeaved] = useState(false);
    

    function onClickAction() {
      let newState = isSelected;
      let newOnceSelected = isOnceSelected;
      if (isEnabled) {
        newState = !isSelected;
        if (!isOnceSelected) {
          newOnceSelected = true;
        }
      }
      setIsSelected(newState);
      setIsOnceSelected(newOnceSelected);
    }

    function onEnterAction() {
      setIsHovered(true&&isOnceSelected&&isOnceLeaved);  
    }

    function onLeaveAction(){
      let newOnceLeaved = isOnceLeaved;
      if (!isOnceLeaved) {
        newOnceLeaved = true;
      }
      setIsOnceLeaved(newOnceLeaved);
      setIsHovered(false);
    }

    function buildFooter(){
      if (isEnabled && !isSelected) {
        return (
          <>
            { props.desc_bottom } <a href="#" onClick={onClickAction}>купи.</a>
          </>
        );
      }
      if (isEnabled && isSelected) {
        return (
          <>
            { props.desc_selected }
          </>
        )
      }
      if (!isEnabled) {
        return (
          <>
            {props.desc_disabled}
          </>
        )
      }
    }

    function addClassName(base) {
      let addBase = ' '+base;
      return base + ( isEnabled && isSelected && isHovered ? addBase+'-selected-hovered ' :'') +
             ( isEnabled && isSelected && !isHovered? addBase+'-selected' : '') +
             ( isEnabled && !isSelected && isHovered? addBase+'-hovered': '') +
             ( !isEnabled ? addBase+'-disabled' : '')
    };

    return (
      <div className="Card-wrap">
          <div className = {addClassName('Card')}>
            <div className={addClassName('Card-content')}
                 onClick={onClickAction}
                 onMouseEnter={onEnterAction}
                 onMouseLeave={onLeaveAction}>
                <div className="Card-content-wrap">
                  <h3>{props.desc_up}</h3> 
                  <div className="Card-header-wrap">
                    <h1>{props.header_txt[0]}</h1>  
                    <h2>{props.header_txt[1]}</h2>
                  </div>
                  {
                    props.desc.map((item, index) => (
                      <h4 key={index}>{item}</h4> 
                    ))
                  }
                </div>
                <div className={addClassName('Card-circle')}>
                  <div>
                    <span className='Card-circle-top'>{props.desc_weight[0]}</span>
                  </div>
                  <div>
                    <span className='Card-circle-bottom'>{props.desc_weight[1]}</span>
                  </div>
                </div>  
            </div> 
          </div>
          <div className="Card-footer">
            <p className= {!isEnabled ? 'Card-footer-disabled': ''}> {buildFooter()} </p>
          </div>
       </div>
    );
}

Card.defaultProps = {
  isEnabled: true,
  isSelected: false
}

export default Card;
