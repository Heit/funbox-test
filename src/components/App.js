import React from 'react';
import '../styles/App.css';

import Card from './Card.js';

import cardConfig from '../config/card-config.js';

function App() {
  return (
    <>  
      <div className="flexwrap">
        <div className="flexbox">
          <div className="flexheader">
            <h1>Ты сегодня покормил кота?</h1>  
          </div>
          {
            cardConfig.map((item, index) => (
              <Card key={index} {...item} />
            ))
          }
        </div>
      </div>  
    </>
  );
}

export default App;
