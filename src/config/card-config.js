const cardConfig = [
    {
        id: "card.foie",
        desc_up: "Сказочное заморское яство",
        header_txt: ["Нямушка", "с фуа-гра"],
        desc: ["10 порций", "мышь в подарок"],
        desc_weight: ["0,5","КГ"],
        desc_bottom: "Чего сидишь? порадуй котэ,",
        desc_selected:  "Печень утки разварная с артишоками.",
        desc_disabled: "Печалька, с фуа-гра закончился.",
        isEnabled: true
    },
    {
        id: "card.fish",
        desc_up: "Сказочное заморское яство",
        header_txt: ["Нямушка", "с рыбой"],
        desc: ["40 порций", "2 мыши в подарок"] ,
        desc_weight:  ["2","КГ"],
        desc_bottom: "Чего сидишь? порадуй котэ,",
        desc_selected:  "Головы щучьи с чесноком да свежайшая сёмгугушка",
        desc_disabled: "Печалька, с рыбой закончился.",
        isEnabled: true
    },
    {
        id: "card.chicken",
        desc_up: "Сказочное заморское яство",
        header_txt: ["Нямушка","с курой"],
        desc: ["100 порций", "5 мышей в подарок", "заказчик доволен"],
        desc_weight:  ["5","КГ"],
        desc_bottom: "Чего сидишь? порадуй котэ,",
        desc_selected:  "Филе из цыплят с трюфелями в бульоне.",
        desc_disabled: "Печалька, с курой закончился.",
        isEnabled: false
    }
];

export default cardConfig;